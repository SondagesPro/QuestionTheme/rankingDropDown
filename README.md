# LimeSurvey uestion theme : rankingDropDown

Show simple dropdown for ranking directly. More easy to use than the core system.

## Usage

No new attribute is set, system use same HTML than default LimeSurvey, just disable the javascript to create sortbale view.

## Installation and sage

1. Download and extract 
2. Upload the "rankingDropDown" folder to ./upload/themes/question/.
3. Create an Ranking with drop down question

![Sample](survey/questions/answer/ranking/assets/ranking_dropdown.png?raw=true "Sample view with alert.")


## Copyright and home page

- HomePage <https://extensions.sondages.pro>
- Support <https://support.sondages.pro>
- Main repository, contribution and bug report <https://gitlab.com/SondagesPro/QuestionTheme/rankingDropDown>
- Copyright © 2021-2024 Denis Chenu <https://sondages.pro>
- Licence : GNU General Public License <https://www.gnu.org/licenses/gpl-3.0.html>
- [Donate directly](https://support.sondages.pro/open.php?topicId=12) [Donate on Liberapay](https://liberapay.com/SondagesPro/) 
